import { combineReducers } from 'redux';

import catalog from '../modules/home/HomeState'
import cart from '../modules/cart/cartProduct/CartProductState'
import user from '../modules/profile/personalArea/PersonalAreaState'

const rootReducer = combineReducers({
    catalog,
    cart,
    user
});

export default rootReducer
