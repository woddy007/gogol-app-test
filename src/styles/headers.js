import {StyleSheet} from "react-native";

const headers = StyleSheet.create({
    headerContainer: {
        height: 80,
        justifyContent: 'flex-end',
        paddingHorizontal: 14,
        paddingBottom: 10,
    },
    headerCaption: {
        color: '#000000',
        fontWeight: '500',
        fontSize: 16,
        textAlign: 'center',
        flex: 1,
        flexGrow: 1,
    },
    content: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    leftButton: {
        width: 40,
        flex: 1,
        justifyContent: 'flex-end',
    },
    rightButton: {
        width: 40,
        flex: 1,
        justifyContent: 'flex-end',
    },
    buttonBack: {
        width: 40,
        flex: 1,
        justifyContent: 'center',
    },
})

export default headers
