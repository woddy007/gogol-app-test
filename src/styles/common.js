const common = {
    pageStyle: {
        padding: 15,
        backgroundColor: '#F3F5F7',
        flex: 1
    },

    input: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(212, 212, 212, 0.5)',
        borderRadius: 5,

        paddingVertical: 13,
        paddingHorizontal: 10,
    },
}

export default common
