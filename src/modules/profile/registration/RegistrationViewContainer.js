// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import RegistrationView from './RegistrationView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(RegistrationView);
