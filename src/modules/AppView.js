import React, {Component} from 'react';
import Navigator from './navigation/Navigator';
import {AsyncStorage} from "react-native";
import * as Permissions from 'expo-permissions';
import axios from "../plugins/axios";

class AppView extends Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount = () => {
        this.initBasket()
        this.checkUserAuthorization()
    }
    initBasket = async () => {
        const basket = await AsyncStorage.getItem('basket');

        if (basket) {
            let list = JSON.parse(basket)

            for(let key in list){
                let item = list[key]

                let product = item
                let count = item.count

                this.props.addProduct(product, count)
            }
        }
    }
    checkUserAuthorization = async () => {
        const token = await AsyncStorage.getItem('token');
        if (token){
            axios('get', 'api-shop-shopapp/me').then(response => {
                let user = response.data
                this.props.setUser(user)
            })
        }
    }

    render() {
        return (
            <Navigator
                onNavigationStateChange={() => {}}
                uriPrefix="/app"
            />
        )
    }
}

export default AppView
