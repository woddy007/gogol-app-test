import React from 'react';
import {
    Image,
    View,
    StyleSheet,
    Text,
    StatusBar
} from 'react-native';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

import HomeScreen from '../home/HomeViewContainer'
import ProductScreen from '../product/ProductViewContainer'
import ContactScreen from '../contact/ContactViewContainer'
import ProfileInitScreen from '../profile/init/InitViewContainer'
import CartProducts from '../cart/cartProduct/CartProductViewContainer'
import CartOrderScreen from '../cart/cartOrder/CartOrderViewContainer'
import PersonalAreaScreen from '../profile/personalArea/PersonalAreaViewContainer'
import PersonalInformationScreen from '../profile/personalInformation/PersonalInformationViewContainer'
import PurchaseHistoryScreen from '../profile/purchaseHistory/PurchaseHistoryViewContainer'
import DiscountCardScreen from '../profile/discountCard/DiscountCardViewContainer'
import AuthorizationScreen from '../profile/authorization/AuthorizationViewContainer'
import RegistrationScreen from '../profile/registration/RegistrationViewContainer'
import VerificationScreen from '../profile/verification/VerificationViewContainer'


const iconMenu = require('../../../assets/icons/menu.png')
const iconContact = require('../../../assets/icons/phone.png')
const iconProfile = require('../../../assets/icons/user.png')
const iconCart = require('../../../assets/icons/cart.png')

const styles = StyleSheet.create({
    tabBarItemContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 5,
        color: '#7C7C7C',
    },
    tabBarIcon: {
        width: 17,
        height: 17,
        tintColor: '#939393'
    },
    tabBarIconFocused: {
        tintColor: '#C3D600',
    },
    tabBarTitle: {
        color: '#939393',
        fontWeight: '300',
        marginTop: 5,
        fontSize: 10,
    },
    tabBarFocudes: {
        color: '#C3D600',
    },

    badge: {
        position: 'absolute',
        right: 0,
        top: -10,
        width: 20,
        height: 20,
        backgroundColor: '#39B54A',
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    badgeText: {
        color: 'white',
        fontSize: 10
    }
});

const TransitionConfig = () => ({
    screenInterpolator: ({layout, position, scene}) => {
        let pages = [
            'PersonalArea',
            'PersonalInformation',
            'PurchaseHistory',
            'DiscountCard',
        ]
        let routeName = scene.route.routeName

        if (pages.indexOf(routeName) >= 0) {
            const {index} = scene
            const {initWidth} = layout

            const translateX = position.interpolate({
                inputRange: [index - 1, index, index + 1],
                outputRange: [initWidth, 0, -initWidth],
            })

            return {
                transform: [{translateX}],
            }
        }
    }
})

export default createBottomTabNavigator(
    {
        Catalog: {
            screen: createStackNavigator({
                Home: {
                    screen: HomeScreen,
                    navigationOptions: {
                        headerStyle: {
                            backgroundColor: '#C3D600',
                            borderBottomWidth: 0,
                            elevation: 0,
                            shadowOpacity: 0,
                        },
                        headerBackTitleStyle: {
                            tintColor: 'white'
                        },
                        headerTintColor: 'white'
                    }
                },
                Product: {
                    screen: ProductScreen,
                    navigationOptions: {
                        headerStyle: {
                            backgroundColor: '#C3D600',
                            borderBottomWidth: 0,
                            elevation: 0,
                            shadowOpacity: 0,
                        },
                        headerBackTitleStyle: {
                            tintColor: 'white'
                        },
                        headerTintColor: 'white'
                    }
                },
            })
        },
        Cart: {
            screen: createStackNavigator({
                Cart: {
                    screen: CartProducts,
                    navigationOptions: {
                        headerStyle: {
                            backgroundColor: '#C3D600',
                            borderBottomWidth: 0,
                            elevation: 0,
                            shadowOpacity: 0,
                        },
                        headerBackTitleStyle: {
                            tintColor: 'white'
                        },
                        headerTintColor: 'white'
                    }
                },
                CartOrder: {
                    screen: CartOrderScreen,
                    navigationOptions: {
                        headerStyle: {
                            backgroundColor: '#C3D600',
                            borderBottomWidth: 0,
                            elevation: 0,
                            shadowOpacity: 0,
                        },
                        headerBackTitleStyle: {
                            tintColor: 'white'
                        },
                        headerTintColor: 'white'
                    }
                }
            })
        },
        Contact: {
            screen: createStackNavigator({
                Contact: {
                    screen: ContactScreen,
                    navigationOptions: {
                        headerStyle: {
                            backgroundColor: '#C3D600',
                            borderBottomWidth: 0,
                            elevation: 0,
                            shadowOpacity: 0,
                        },
                        headerBackTitleStyle: {
                            tintColor: 'white'
                        },
                        headerTintColor: 'white'
                    }
                },
            })
        },
        Profile: {
            screen: createStackNavigator({
                    Init: {
                        screen: ProfileInitScreen,
                        navigationOptions: {
                            headerShown: false
                        }
                    },
                    Authorization: {
                        screen: AuthorizationScreen,
                        navigationOptions: {
                            headerShown: false
                        }
                    },
                    Registration: {
                        screen: RegistrationScreen,
                        navigationOptions: {
                            headerShown: false
                        }
                    },
                    Verification: {
                        screen: VerificationScreen,
                        navigationOptions: {
                            headerShown: false
                        }
                    },
                    PersonalArea: {
                        screen: PersonalAreaScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#C3D600',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white'
                        }
                    },
                    PersonalInformation: {
                        screen: PersonalInformationScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#C3D600',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white'
                        }
                    },
                    PurchaseHistory: {
                        screen: PurchaseHistoryScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#C3D600',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white'
                        }
                    },
                    DiscountCard: {
                        screen: DiscountCardScreen,
                        navigationOptions: {
                            headerStyle: {
                                backgroundColor: '#C3D600',
                                borderBottomWidth: 0,
                                elevation: 0,
                                shadowOpacity: 0,
                            },
                            headerBackTitleStyle: {
                                tintColor: 'white'
                            },
                            headerTintColor: 'white'
                        }
                    },
                },
                {transitionConfig: TransitionConfig}
            )
        },
    },
    {
        defaultNavigationOptions: ({navigation, screenProps}) => ({
            tabBarIcon: ({focused}) => {
                const {routeName} = navigation.state;
                let iconSource;

                switch (routeName) {
                    case 'Catalog':
                        iconSource = iconMenu;
                        title = 'Каталог';
                        badge = 0;
                        break;
                    case 'Contact':
                        iconSource = iconContact;
                        title = 'Контакты';
                        badge = 0;
                        break;
                    case 'Profile':
                        iconSource = iconProfile;
                        title = 'Профиль';
                        badge = 0;
                        break;
                    case 'Cart':
                        iconSource = iconCart;
                        title = 'Корзина';
                        badge = screenProps.countCart;
                        break;
                    default:
                        iconSource = iconMenu;
                }
                return (
                    <View style={styles.tabBarItemContainer}>
                        <Image
                            resizeMode="contain"
                            source={iconSource}
                            style={[styles.tabBarIcon, focused && styles.tabBarIconFocused]}
                        />
                        {
                            badge ? <View style={styles.badge}><Text style={styles.badgeText}>{badge}</Text></View> :
                                <View></View>
                        }
                        <Text style={[styles.tabBarTitle, focused && styles.tabBarFocudes]}>{title}</Text>
                    </View>
                );
            },
        }),
        initialRouteName: 'Catalog',
        tabBarPosition: 'bottom',
        animationEnabled: true,
        swipeEnabled: true,
        tabBarOptions: {
            showIcon: true,
            showLabel: false,
            style: {
                backgroundColor: '#FFFFFF',
            },
            labelStyle: {
                color: '#7C7C7C',
                fontSize: 10,
                lineHeight: 12,
                padding: 0,
                margin: 0,
                fontWeight: '300'
            },
        },
        navigationOptions: {
            headerBackTitle: null
        }
    },
);
