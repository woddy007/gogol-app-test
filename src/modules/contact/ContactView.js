import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    ActivityIndicator
} from 'react-native';
import { Linking } from 'expo';
import axios from "../../plugins/axios";


class Contact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showMap: false,
            config: {}
        }
    }

    componentDidMount = () => {
        this.initData()
        setTimeout(() => {
            this.setState({showMap: true})
        }, 1000)
    }

    callShop = () => {
        let phone = this.state.config.phone

        if (phone){
            phone = parseInt(phone.replace(/\D+/g,""));
        }

        Linking.openURL('tel:' + phone);
    }
    initData = () => {
        axios('get', 'api-shop-shopapp/getcontacts').then(response => {
            console.log('responseL ', response)

            this.setState({
                config: response.data
            })
        }).catch(error => {
            console.log('error: ', error.response)
        })
    }

    render() {
        let phone = this.state.config.phone
        return (
            <ScrollView style={styles.page}>
                <View style={styles.infoContent}>
                    <Text style={styles.infoText}>ООО «Волкова и К»</Text>
                    <Text style={styles.infoText}>
                        Офис:
                        <Text style={styles.infoTextGrey}>
                            620075, г. Екатеринбург,
                            ул. Мамина-Сибиряка д. 101, оф. 9.03
                            (Пн-Пт: 10:00 - 18:00)
                        </Text>
                    </Text>
                    <Text style={styles.infoText}>
                        Производство:
                        <Text style={styles.infoTextGrey}>
                            620041, г. Екатеринбург,
                            ул. Московская, д. 39
                        </Text>
                    </Text>
                    {
                        (phone) && <TouchableWithoutFeedback
                            onPress={() => this.callShop()}
                        >
                            <View style={styles.buttonPhone}>
                                <Text style={styles.buttonPhoneText}>Позвонить в магазин</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    }
                </View>
            </ScrollView>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Контакты',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    mapContent: {
        height: 250,
    },
    infoContent: {
        padding: 16
    },
    infoText: {
        marginBottom: 10,
        fontSize: 16
    },
    infoTextGrey: {
        color: '#666666'
    },

    buttonPhone: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: '#C3D600',
        alignItems: 'center',
        marginTop: 36
    },
    buttonPhoneText: {
        color: '#FFFFFF',
        fontSize: 18
    }
})

export default Contact

// Производство:
