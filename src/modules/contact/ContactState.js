// @flow
type ContactStateType = {};

type ActionType = {
  type: string,
  payload?: any,
};

export const initialState: ContactStateType = {};

export const ACTION = 'ContactState/ACTION';

export function actionCreator(): ActionType {
  return {
    type: ACTION,
  };
}

export default function ContactStateReducer(state: ContactStateType = initialState, action: ActionType): ContactStateType {
  switch (action.type) {
    case ACTION:
      return {
        ...state,
      };
    default:
      return state;
  }
}
