// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ContactView from './ContactView';

export default compose(
  connect(
    state => ({}),
    dispatch => ({}),
  ),
)(ContactView);
