// @flow
import { compose } from 'recompose';
import { connect } from 'react-redux';

import ProductView from './ProductView';
import {addProduct} from "../cart/cartProduct/CartProductState";

export default compose(
  connect(
    state => ({}),
    dispatch => ({
        addProduct: (product, count) => dispatch(addProduct(product, count))
    }),
  ),
)(ProductView);
