import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Image,
    TouchableWithoutFeedback,
    Dimensions
} from 'react-native';
import axios from "../../plugins/axios";
import MarkdownContainer from "../../components/Markdown";
import {DropDownHolder} from "../../components/DropDownAlert";

class Product extends Component {
    constructor(props) {
        super(props);

        this.state = {
            productId: null,
            openDescription: false,
            count: 1,

            timeIntervar: 500,

            product: null
        }

        this.gradualIncreaseCount = null
        this.gradualDecreaseCount = null
    }

    componentDidMount = () => {
        this.setState({
            productId: this.props.navigation.state.params.id
        })

        this.wait(500).then(() => {
            this.loadProductData()
        })
    }

    wait = (timeout) => {
        return new Promise(resolve => {
            setTimeout(resolve, timeout);
        });
    }

    loadProductData = () => {
        axios('get', 'api-shop-shopapp/product?id=' + this.state.productId).then(response => {
            this.setState({
                product: response.data
            })
        }).catch(error => {
            console.log('error: ', error.response)
        })
    }

    addProduct = () => {
        let product = this.state.product
        let count = this.state.count

        this.props.addProduct(product, count)

        DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Вы добавили товар в корзину.');

        this.setState({
            count: 1
        })
    }

    addCountProduct = () => {
        if (this.state.product.quantity >= this.state.count + 1) {
            this.setState({count: this.state.count + 1})
        }
    }
    downCountProduct = () => {
        if (this.state.count > 1) {
            this.setState({count: this.state.count - 1})
        } else {
            this.gradualDecreaseCountEnd()
        }
    }

    gradualIncreaseCountStart = () => {
        this.addCountProduct()

        this.gradualIncreaseCount = setInterval(() => {
            this.addCountProduct()
        }, this.state.timeIntervar)
    }
    gradualIncreaseCountEnd = () => {
        clearInterval(this.gradualIncreaseCount)
    }

    gradualDecreaseCountStart = () => {
        this.downCountProduct()

        this.gradualDecreaseCount = setInterval(() => {
            this.downCountProduct()
        }, this.state.timeIntervar)
    }
    gradualDecreaseCountEnd = () => {
        clearInterval(this.gradualDecreaseCount)
    }

    getCharacteristics = (characteristics) => {
        let list = []

        for(let key in characteristics){
            let item = characteristics[key]

            list.push({
                name: item.name,
                value: item.values[0]
            })
        }

        return list
    }

    render() {
        let product = this.state.product
        return (
            <View style={styles.page}>
                {
                    (this.state.product) ? <ScrollView
                            style={{padding: 16}}
                            showsVerticalScrollIndicator={false}
                        >
                            <View style={styles.productImageContent}>
                                <Image
                                    style={styles.productImage}
                                    source={{uri: product.photos[0]['src']}}
                                    resizeMode="contain"
                                />
                            </View>
                            {
                                (product.category) && <Text style={styles.productCategory}>{product.category.name}</Text>
                            }
                            <Text style={styles.productName}>{product.name}</Text>
                            <View style={styles.productLineInfo}>
                                <View style={styles.productCountView}>
                                    <TouchableWithoutFeedback
                                        onPress={this.downCountProduct}
                                        onPressOut={() => this.gradualDecreaseCountEnd()}
                                        onLongPress={() => this.gradualDecreaseCountStart()}
                                    >
                                        <View style={styles.productButtonCount}>
                                            <Text style={styles.productButtonCountText}>-</Text>
                                        </View>
                                    </TouchableWithoutFeedback>

                                    <Text style={styles.productTextCount}>{this.state.count}шт</Text>

                                    <TouchableWithoutFeedback
                                        onPress={this.addCountProduct}
                                        onPressOut={() => this.gradualIncreaseCountEnd()}
                                        onLongPress={() => this.gradualIncreaseCountStart()}
                                    >
                                        <View style={[styles.productButtonCount, {backgroundColor: '#C3D600'}]}>
                                            <Text style={[styles.productButtonCountText, {color: 'white'}]}>+</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                                <Text style={styles.productPrice}>{Number(product.price) * this.state.count}₽</Text>
                            </View>
                            <View style={{alignItems: 'center', marginBottom: 10}}>
                                <TouchableWithoutFeedback
                                    onPress={() => this.addProduct()}
                                >
                                    <View style={styles.productButtonBuy}>
                                        <Text style={styles.productButtonBuyText}>Добавить в корзину</Text>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>

                            <View style={{marginBottom: 18}}>
                                <MarkdownContainer>
                                    { product.description }
                                </MarkdownContainer>
                            </View>

                            <View style={styles.contentCharacteristics}>
                                {
                                    this.getCharacteristics(product.characteristics).map((item, idx) => {
                                        return(
                                            <View
                                                style={styles.characteristic}
                                                key={'characteristic-' + idx}
                                            >
                                                <Text style={styles.characteristicLabel}>
                                                    { item.name }:
                                                </Text>
                                                <Text style={styles.characteristicValue}>
                                                    { item.value }
                                                </Text>
                                            </View>
                                        )
                                    })
                                }
                            </View>
                        </ScrollView> :
                        <View style={{padding: 16}}>
                            <View style={[styles.productImageContent, preloader.productImageContent]}></View>
                            <Text style={[styles.productCategory, preloader.productCategory]}></Text>
                            <Text style={[styles.productName, preloader.productName]}></Text>
                            <View style={[styles.productLineInfo, preloader.productLineInfo]}></View>
                        </View>
                }
            </View>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Продукт',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    productImageContent: {
        height: 200,
        width: '100%',
        marginBottom: 20
    },
    productImage: {
        flex: 1,
    },
    productCategory: {
        textTransform: 'uppercase',
        color: '#666666',
        fontSize: 16,
        fontWeight: '600'
    },
    productName: {
        textTransform: 'uppercase',
        color: '#C3D600',
        fontSize: 22,
        fontWeight: '600',
        marginBottom: 17
    },

    productLineInfo: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15
    },

    productButtonCount: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#C3D600',
    },
    productButtonCountText: {
        fontSize: 26,
        lineHeight: 30,
        color: '#C3D600'
    },
    productTextCount: {
        fontWeight: '300',
        color: '#666666',
        fontSize: 18,
        marginHorizontal: 7
    },
    productCountView: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    productPrice: {
        fontSize: 26,
        fontWeight: '500',
        color: '#C3D600',
        marginLeft: 50
    },

    productContentDescription: {},
    productTextDescription: {
        color: '#666666',
        fontWeight: '300',
        marginBottom: 5
    },

    productButtonBuy: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: '#C3D600',
        alignItems: 'center'
    },
    productButtonBuyText: {
        color: '#FFFFFF',
        fontSize: 18
    },

    contentCharacteristics: {
        marginBottom: 20
    },
    characteristic: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginBottom: 3
    },
    characteristicLabel: {
        color: '#666666',
        fontWeight: '600',
        fontSize: 14,
        width: '40%'
    },
    characteristicValue: {
        color: '#666666',
        fontWeight: '300',
        fontSize: 14
    }
})
const preloader = StyleSheet.create({
    productImageContent: {
        backgroundColor: '#d4d4d4',
        borderRadius: 5
    },
    productCategory: {
        height: 22,
        backgroundColor: '#d4d4d4',
        borderRadius: 5,
        marginBottom: 17
    },
    productName: {
        height: 30,
        backgroundColor: '#d4d4d4',
        borderRadius: 5
    },
    productLineInfo: {
        height: 35,
        backgroundColor: '#d4d4d4',
        borderRadius: 5
    },
})

const collectiveMd = {
    heading1: {
        color: 'red'
    },
    heading2: {
        color: 'green',
        textAlign: "right"
    },
    strong: {
        color: 'blue'
    },
    em: {
        fontFamily: '',
        color: 'cyan'
    },
    text: {
        color: 'black',
    },
    blockQuoteText: {
        color: 'grey'
    },
    blockQuoteSection: {
        flexDirection: 'row',
    },
    blockQuoteSectionBar: {
        width: 3,
        height: null,
        backgroundColor: '#DDDDDD',
        marginRight: 15,
    },
    codeBlock: {
        fontFamily: '',
        fontWeight: '500',
        backgroundColor: '#DDDDDD',
    },
    tableHeader: {
        backgroundColor: 'grey',
    },
    inlineCode: {
        backgroundColor: 'rgba(128, 128, 128, 0.25)',
        fontFamily: '1',
        fontWeight: '500',
    },
    monospace: {
        fontFamily: '',
        fontSize: 11,
    },
}

export default Product
