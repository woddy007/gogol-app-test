import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    TouchableWithoutFeedback,
    TextInput,
    KeyboardAvoidingView
} from 'react-native';
import common from "../../../styles/common";
import {TextInputMask} from "react-native-masked-text";
import {getData} from '../cartProduct/CartProductState'
import {DropDownHolder} from "../../../components/DropDownAlert";
import axios from "../../../plugins/axios";
import {getTotalPrice} from '../cartProduct/CartProductState'

class CartOrder extends Component {
    constructor(props) {
        super(props);

        this.state = {
            behavior: 'padding',
            fullName: '',
            phone: '',
            address: '',
            userComment: '',

            delivery: null,
            payments: null,

            listDelivery: [
                {
                    id: 5,
                    name: 'Самовывоз',
                },
                {
                    id: 1,
                    name: 'Доставка по Екатеринбургу',
                },
                {
                    id: 4,
                    name: 'Курьерская доставка (СДЭК)',
                },
            ],
            listPayments: [
                {
                    id: 4,
                    name: 'Оплата при получении',
                }
            ]
        }
    }

    componentDidMount = () => {
        this.writeUserData()
    }
    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (prevProps.user.user != this.props.user.user) {
            this.writeUserData()
        }
    }

    order = () => {
        let isValidation = this.isValidation()

        if (isValidation) {
            let order = {}
            let customerForm = this.getUserOrder()
            let products = this.getProductOrder()
            let deliveryForm = this.getDeliveryOrder()
            let paymentFormId = 1

            order['PaymentForm'] = {
                id: paymentFormId
            }
            order['DeliveryForm'] = deliveryForm
            order['CustomerForm'] = customerForm
            order['note'] = this.state.userComment
            order['products'] = products

            this.clearOrder()

            axios('post', 'api-shop-shopapp/checkout', order).then(response => {
                this.clearOrder()
                DropDownHolder.dropDown.alertWithType('success', 'Успешно', 'Заказ оформлен');
            }).catch(error => {
                DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Ошибка сервера');
            })
        } else {
            DropDownHolder.dropDown.alertWithType('error', 'Ошибка', 'Заполните все поля.');
        }
    }

    writeUserData = () => {
        let {user} = this.props.user

        if (user) {
            this.setState({
                fullName: user.name,
                phone: user.phone,
                address: user.address,
            })
        } else {
            this.setState({
                fullName: '',
                phone: '',
                address: '',
            })
        }
    }
    isValidation = () => {
        let bool = true
        let phone = String(parseInt(this.state.phone.replace(/\D+/g, "")))

        if (!this.state.delivery) {
            bool = false
        }

        if (this.state.delivery == 1 || this.state.delivery == 4) {
            if (!this.state.address) {
                bool = false
            }
        }

        if (!this.state.payments) {
            bool = false
        }

        if (!this.state.fullName) {
            bool = false
        }

        if (!this.state.phone || phone.length != 11) {
            bool = false
        }

        return bool
    }
    getUserOrder = () => {
        let user = {
            name: '',
            phone: '',
            email: '',
            legal_type: '',
            organization_name: '',
        }

        if (!this.props.user.user) {
            user.name = this.state.fullName
            user.phone = this.state.phone
        } else {
            let data = this.props.user.user

            user.name = data.name
            user.phone = data.phone
            user.email = data.email
            user.legal_type = data.legal_type
            user.organization_name = data.organization_name
        }

        return user
    }
    getProductOrder = () => {
        let list = this.props.cart.list
        let products = []

        for (let key in list) {
            let product = list[key]

            products.push({
                "quantity": product.count,
                "product_id": product.id,
                "modification": 0
            })
        }

        return products
    }
    getDeliveryOrder = () => {
        let delivery = this.state.delivery
        let address = this.state.address

        let data = {
            id: delivery,
        }

        if (delivery == 5) {
            data['shop_id'] = 1
        }

        if (delivery == 1 || delivery == 4) {
            data['address'] = address
        }

        return data
    }
    clearOrder = () => {
        const {navigate} = this.props.navigation;

        this.setState({
            delivery: null,
            payments: null,
            fullName: '',
            phone: '',
            address: '',
            userComment: '',
        })

        this.props.clearCart()

        navigate('Home')
    }

    render() {
        return (
            <KeyboardAvoidingView
                behavior="position"
                enabled
                style={styles.page}
            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{padding: 15}}
                >
                    <View style={{marginBottom: 20}}>
                        <Text style={styles.label}>Выберите способ доставки</Text>
                        {
                            this.state.listDelivery.map((item, idx) => {
                                return (
                                    <View
                                        key={'delivery-type-' + idx}
                                    >
                                        <TouchableWithoutFeedback
                                            onPress={() => {
                                                this.setState({delivery: item.id})
                                            }}
                                        >
                                            <View style={styles.radio}>
                                                <View
                                                    style={[styles.radioEvent, (this.state.delivery == item.id) ? styles.radioEventActive : '']}></View>
                                                <Text
                                                    style={[styles.radioText, (this.state.delivery == item.id) ? styles.radioTextActive : '']}>{item.name}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                )
                            })
                        }

                        {
                            (this.state.delivery == 1 || this.state.delivery == 4) && <View style={{marginTop: 15}}>
                                {
                                    (this.state.delivery == 1) && <Text style={styles.description}>
                                        В Екатеринбурге доставляем заказы силами собственной курьерской службы. На
                                        покупку от 1500
                                        рублей действует бесплатная доставка в пределах города.
                                    </Text>
                                }
                                <TextInput
                                    value={this.state.address}
                                    style={[common.input, styles.input]}
                                    placeholder="Улица, дом, этаж, квартира"
                                    onChangeText={(address) => {
                                        this.setState({address})
                                    }}
                                    placeholderTextColor="#D5D5D5"
                                    multiline
                                />
                            </View>
                        }
                    </View>
                    <View style={{marginBottom: 20}}>
                        <Text style={styles.label}>Выберите способ оплаты</Text>
                        {
                            this.state.listPayments.map((item, idx) => {
                                return (
                                    <View
                                        key={'payment-' + idx}
                                    >
                                        <TouchableWithoutFeedback
                                            onPress={() => {
                                                this.setState({payments: item.id})
                                            }}
                                        >
                                            <View style={styles.radio}>
                                                <View
                                                    style={[styles.radioEvent, (this.state.payments == item.id) ? styles.radioEventActive : '']}></View>
                                                <Text
                                                    style={[styles.radioText, (this.state.payments == item.id) ? styles.radioTextActive : '']}>{item.name}</Text>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                )
                            })
                        }
                    </View>
                    <View style={{marginBottom: 20}}>
                        <Text style={styles.label}>Контактная информация</Text>
                        <TextInput
                            value={this.state.fullName}
                            style={[common.input, styles.input]}
                            placeholder="Имя"
                            onChangeText={(fullName) => {
                                this.setState({fullName})
                            }}
                            placeholderTextColor="#D5D5D5"
                        />
                        <TextInputMask
                            type={'custom'}
                            options={{
                                mask: '+7(999)999-99-99',
                            }}
                            value={this.state.phone}
                            style={[common.input, styles.input]}
                            placeholder="Контактный телефон"
                            onChangeText={(phone) => {
                                this.setState({phone})
                            }}
                            placeholderTextColor="#D5D5D5"
                            keyboardType="number-pad"
                        />
                        <TextInput
                            value={this.state.userComment}
                            style={[common.input, styles.input]}
                            placeholder="Комментарий"
                            onChangeText={(userComment) => {
                                this.setState({userComment})
                            }}
                            placeholderTextColor="#D5D5D5"
                            multiline
                        />
                    </View>

                    <View style={styles.orderInfo}>
                        <View style={styles.orderInfoLine}>
                            <Text style={[styles.orderInfoLineText, styles.orderInfoLineTextBold]}>Итого</Text>
                            <Text
                                style={[styles.orderInfoLineText, styles.orderInfoLineTextBold]}>{getTotalPrice()} ₽</Text>
                        </View>
                    </View>

                    <TouchableWithoutFeedback
                        onPress={() => this.order()}
                    >
                        <View style={[styles.buttonSave]}>
                            <Text style={styles.buttonSaveText}>Сделать заказ</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }

    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Оформление заказа',
        };
    };
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },

    description: {
        fontSize: 16,
        lineHeight: 20,
        color: '#8C8C8C',
        marginBottom: 10
    },

    input: {
        marginBottom: 10
    },
    label: {
        fontSize: 18,
        color: '#666666',
        marginBottom: 5
    },

    radio: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5
    },
    radioEvent: {
        width: 15,
        height: 15,
        borderRadius: 20,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#C3D600',
        marginRight: 5
    },
    radioEventActive: {
        backgroundColor: '#C3D600'
    },
    radioText: {
        fontSize: 16,
        color: '#D4D4D4',
    },
    radioTextActive: {
        color: '#C3D600',
    },

    orderInfo: {
        flex: 1,
        marginBottom: 20
    },
    orderInfoLine: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    orderInfoLineText: {
        fontSize: 20,
        fontWeight: '700',
        color: '#666666'
    },
    orderInfoLineTextBold: {
        fontSize: 20,
        fontWeight: '700',
        color: '#666666'
    },

    buttonSave: {
        width: '100%',
        padding: 10,
        borderRadius: 3,
        backgroundColor: '#C3D600',
        alignItems: 'center'
    },
    buttonSaveText: {
        color: '#FFFFFF',
        fontSize: 18
    }
})

export default CartOrder
