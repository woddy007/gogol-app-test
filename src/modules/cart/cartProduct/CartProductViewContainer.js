// @flow
import {compose} from 'recompose';
import {connect} from 'react-redux';

import CartProductView from './CartProductView';
import {changeCountProduct, clearCart, deleteProduct} from './CartProductState'

export default compose(
    connect(
        state => ({
            cart: state.cart
        }),
        dispatch => ({
            changeCountProduct: (id, count) => dispatch(changeCountProduct(id, count)),
            clearCart: () => dispatch(clearCart()),
            deleteProduct: (id) => dispatch(deleteProduct(id))
        }),
    ),
)(CartProductView);
